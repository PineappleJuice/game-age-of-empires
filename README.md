# Content 
Age of Empires  
- II HD The Forgotten
    - Language: CHS
    - OS: Windows

# How to run it 
- Install [游侠对战平台正式版首页](http://pk.ali213.net/)
- Install `./游戏必需运行库/DirectX/Jun2010/DXSETUP.exe`
- Run `./AoK HD-LAN.exe` -> `Ctrl-C`

# Reference 
[Age of Empires](https://en.wikipedia.org/wiki/Age_of_Empires) is a series of historical real-time strategy video games, originally developed by Ensemble Studios and published by Microsoft Studios. The first title of the series was Age of Empires, released in 1997. Seven titles and three spin-offs have been released.  

Age of Empires focused on events in Europe, Africa and Asia, spanning from the Stone Age to the Iron Age; the expansion game explored the formation and expansion of the Roman Empire. The sequel, Age of Empires II: The Age of Kings, was set in the Middle Ages, while its expansion focused partially on the Spanish conquest of Mexico. The subsequent three games of Age of Empires III explored the early modern period, when Europe was colonizing the Americas and several Asian nations were on the decline. The newest installment, Age of Empires Online, takes a different approach as a free-to-play online game utilizing Games for Windows Live. A spin-off game, Age of Mythology, was set in the same period as the original Age of Empires, but focused on mythological elements of Greek, Egyptian, and Norse mythology. A fourth main installment in the series, Age of Empires IV, is under development.  

The Age of Empires series has been a commercial success, selling over 20 million copies. Critics have credited part of the success of the series to its historical theme and fair play; the artificial intelligence (AI) players fight against has fewer advantages than in many of the series' competitors.  
